CLIENT_VER = "6.0.0.1"

ELITE_CODE = "sub-elite"
JAVACLIENT = "javaclient"

SERVER = "https://zygorguides.com"
SERVER_CONTENT = "https://content.zygorguides.com"
NEWS_FEED = SERVER_CONTENT + "/blog/feed"

DOWNLOAD_BUFFER_SIZE = 1024 # 1KB buffer size
SOCKET_TIMEOUT = 5 # 5 seconds