import logging
import sys

import config

class Game:

	_packagerScripts = {
		"wow": "updater",
		"eso": "updater_eso"
	}

	def __init__(self, abbr, name):
		if abbr is None:
			logging.error("abbr must not be None in Game __init__")
			sys.exit(0)

		elif name is None:
			logging.error("name must not be None in Game __init__")
			sys.exit(0)

		else:
			self.abbr = abbr
			self.name = name
			self.packagerLink = "%s/%s/packager.php" % (config.SERVER_CONTENT, self._packagerScripts[self.abbr])

WOW = Game("wow", "World of Warcraft")
ESO = Game("eso", "The Elder Scrolls Online")
