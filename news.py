print("Loading news.py")

import config

import feedparser
import logging

from datetime import datetime

def getNewsEntries(count=5):
	if count is None:
		count = 5

	feed = feedparser.parse(config.NEWS_FEED)

	wellFormed = feed["bozo"] == 0

	if wellFormed:
		items = feed["items"]

		entries = []
		for item in items:
			title = None
			date = None
			link = None

			if "title" in item:
				title = item["title"]

			if "published" in item:
				date = item["published"]

			if "link" in item:
				link = item["link"]

			if title is not None and date is not None and link is not None:
				entries.append(BlogEntry(title, date, link))

			else:
				logging.error("One of title, date, or link is None: %s, %s, %s" % (title, date, link))

			if len(entries) >= count:
				break

		return entries

	else:
		logging.error("XML feed is not well-formed.")
		if "bozo_exception" in feed:
			raise feed["bozo_exception"]
		else:
			return None

class BlogEntry:

	def __init__(self, title, date, link):
		self.title = title
		self.date = date
		self.link = link

		# Tue, 09 May 2017 20:17:28 +0000
		# Wed, 26 Apr 2017 16:09:14 +0000
		originalFormat = "%a, %d %b %Y %H:%M:%S +0000"
		desiredFormat = "%A, %B %d, %Y"

		self.date = datetime.strptime(self.date, originalFormat).strftime(desiredFormat).lstrip("0").replace(" 0", " ")






