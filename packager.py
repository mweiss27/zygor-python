print("Loading packager.py")

import config
import logging
import sys
import games
import util
import re

from functools import partial
from tempfile import NamedTemporaryFile
from urllib.error import URLError, HTTPError

# Requests and reads the latest version
def getLatestVersion(game):
	if game is None:
		logging.error("game must not be None in getLatestVersion")
		return None

	params = {
		"do": "ver",
		"uv": config.CLIENT_VER,
		"source": "getLatestVersion"
	}

	url = util.constructURL(game.packagerLink, params)

	try:
		conn = util.openConnection(url)
		if conn is not None:
			return util.readHtml(conn.read())

		else:
			logging.error("conn is None in getLatestVersion")
			return None

	except HTTPError as ex:
		logging.error("HTTPError was raised by openConnection in getLatestVersion. Code: %d" % ex.code)
		raise ex

	except URLError as ex:
		logging.error("URLError was raised by openConnection in getLatestVersion. Reason: %s" % ex.reason)
		raise ex

# Requests and reads a Query.
def getQuery(game, user, pw):
	if game is None:
		logging.error("game must not be None in getQuery")
		return None

	if user is None or pw is None:
		logging.error("user and pass must not be None in getQuery: %s, %s" % (user, pw))
		return None

	params = {
		"do": "query",
		"uv": config.CLIENT_VER,
		"source": "getQuery",
		"user": user,
		"pass": pw
	}

	url = util.constructURL(game.packagerLink, params)

	try:
		conn = util.openConnection(url)
		if conn is not None:
			return util.readHtml(conn.read())

		else:
			logging.error("conn is None in getQuery")
			return None

	except HTTPError as ex:
		logging.error("HTTPError was raised by openConnection in getQuery. Code: %d" % ex.code)
		raise ex

	except URLError as ex:
		logging.error("URLError was raised by openConnection in getQuery. Reason: %s" % ex.reason)
		raise ex

def getLicense(game, user, pw):
	if game is None:
		logging.error("game must not be None in getLicense")
		return None

	if user is None or pw is None:
		logging.error("user and pw must not be None in getLicense.")
		return None

	params = {
		"do": "license",
		"user": user,
		"pass": pw,
		"uv": config.CLIENT_VER,
		"source": "getLicense"
	}

	url = util.constructURL(game.packagerLink, params)

	try:
		conn = util.openConnection(url)
		if conn is not None:
			html = util.readHtml(conn.read())
			if html is not None:

				if html == "LOGIN ERR":
					logging.error("Invalid user/pass provided in getLicense.")
					return None

				return html
			else:
				logging.error("conn.read returned None")
				return None

		else:
			logging.error("conn is None in getLicense")
			return None

	except HTTPError as ex:
		logging.error("HTTPError was raised by openConnection in getLicense. Code: %d" % ex.code)
		html = ex.read()
		if html is not None:
			html = html.decode("UTF-8").strip()
			if html == "LOGIN ERR":
				logging.error("Invalid user/pass provided in getLicense.")
				return None

		raise ex

	except URLError as ex:
		logging.error("URLError was raised by openConnection in getLicense. Reason: %s" % ex.reason)
		raise ex

# Requests and downloads a guide update.
# 
# Throws: FailedToCreateTempFile if an error occurs while creating a temp file to download to
# Throws: HTTPError if an error occurs while making the request (includes non 2xx codes)
# Returns: None, if the function was called improperly, or an error occurs in util.openConnection
# 		   A file handle to the .zip file downloaded
def getComp(game, user, pw, files):
	if files is None:
		logging.error("No full DL on mobile")
		return None

	if game is None:
		logging.error("game must not be None in getComp")
		return None

	if user is None or pw is None:
		logging.error("user and pass must not be None in getComp: %s, %s" % (user, pw))
		return None

	tmp = NamedTemporaryFile(delete=False, prefix="zygor_", suffix=".zip")
	if tmp is None:
		logging.error("We failed to create a Temporary File to download the update.")

		from exceptions.FailedToCreateTempFile import FailedToCreateTempFile
		raise FailedToCreateTempFile()
				
	params = {
		"do": "comp",
		"uv": config.CLIENT_VER,
		"source": "getComp",
		"user": user,
		"pass": pw
	}

	data = None
	if files is not None and len(files) > 0:
		logging.debug("Raw files param: %s" % files[0:150])
		data = {
			"list": files
		}

	url = util.constructURL(game.packagerLink, params)

	try:
		conn = util.openConnection(url, data)
		if conn is not None:

			print("Downloading %.2fKB" % (float(conn.length)/1024.0))
			while conn.length > 0:
				try:
					bytes = conn.read(config.DOWNLOAD_BUFFER_SIZE)

					tmp.write(bytes)
				except:
					logging.error("An exception occurred while downloading a comp zip.")
					return None

			tmp.close()
			logging.debug("Returning %s" % tmp)
			return tmp
		else:
			logging.error("conn is None in getComp")
			return None

	except HTTPError as ex:
		logging.error("HTTPError was raised by openConnection in getComp. Code: %d" % ex.code)
		raise ex

	except URLError as ex:
		logging.error("URLError was raised by openConnection in getComp. Reason: %s" % ex.reason)
		raise ex

# Gathers all files (recursively) and generates a String formatted as such:
# F <relative path from AddOns>##<MD5>\r\nF ...
def generateFilesList(addonsDir, productDirs=None):
	if addonsDir is None:
		logging.error("addonsDir must not be None in generateFilesList")
		return ""

	if productDirs is None:
		productDirs = [ "ZygorGuidesViewer" ] # Default

	result = ""
	for productDir in productDirs:
		root = addonsDir + "/" + productDir
		files = util.getAllFiles(root)

		for f in files:
			relative = util.getRelativePath(f, to=addonsDir)
			md5 = util.md5(f)
			if md5 is not None:
				result += "F " + relative + "##" + md5 + "\r\n"

			else:
				logging.error("Error computing md5 of file: %s" % f)

	return result











