import logging
import re

import config

class Query:

	def __init__(self, query):
		if query is None:
			logging.error("query must not be None in Query.__init__")

		else:
			self._query = query
			self._parse()

	def _parse(self):
		if self._query is not None:

			self.productDirs = []
			self.codes = {}
			self.status = "N/A"

			statusPattern = re.compile(r"LOGIN (.*)")
			productPattern = re.compile(r"PRODUCT ([^\s]+)\s(.*)")
			productDirPattern = re.compile(r"PRODUCTDIR ([^\s]+)")

			for line in self._query.split("\n"):
				match = productPattern.match(line)
				if match is not None:
					code = match.group(1)
					product = match.group(2)
					logging.debug("Found a PRODUCT in Query._parse: %s, %s" % (code, product))

					# This is a base code, not an actual product
					if code == "zgv":
						continue

					self.codes[code] = product

				else:
					match = productDirPattern.match(line)
					if match is not None:
						productDir = match.group(1)
						logging.debug("Found a PRODUCTDIR in Query._parse: %s" % productDir)

						self.productDirs.append(productDir)

					else:
						match = statusPattern.match(line)
						if match is not None:
							self.status = match.group(1)
							logging.debug("Found a LOGIN in Query._parse: %s" % self.status)


	# Returns a 3-tuple: ( productAdded, productRemoved, eliteToggled ), all of type bool
	def compare(self, to=None):
		if to is None:
			logging.error("to must not be None in Query.compare")
			return (False, False, False)
		elif type(to) != Query:
			logging.error("to must be of type Query in Query.compare")
			return (False, False, False)

		productAdded = False
		productedRemoved = False
		eliteToggled = False

		for code in self.codes:
			if not code.endswith("-t") and not code.endswith("-trial"):
				if code not in to.codes:
					# A product exists in self, but not to. It was removed
					productedRemoved = True
					break

		for code in to.codes:
			if not code.endswith("-t") and not code.endswith("-trial"):
				if code not in self.codes:
					# A product exists in to, but not self. It was added
					productAdded = True
					break

		if (config.ELITE_CODE in self.codes) ^ (config.ELITE_CODE in to.codes):
			# The presence of sub-elite is different in self and to. Elite was toggled
			eliteToggled = True

		return (productAdded, productedRemoved, eliteToggled)











