import unittest

import os
import shutil
import sys
sys.path.append("..")

import util
import games
import packager

class TestComp(unittest.TestCase):

	username = "Shenzai"
	password = "QöbG9scGFuZGE/"

	currentDir = os.path.dirname(os.path.realpath(__file__))
	compTestDir = currentDir + "/CompTest/"
	zgvDir = compTestDir + "/ZygorGuidesViewer"

	fullSize = -1
	cache = {}

	# This will test a do=comp call given that NO guides are installed
	def test_fullDownload(self):
		if os.path.exists(self.zgvDir):
			shutil.rmtree(self.zgvDir)

		files = packager.generateFilesList(self.compTestDir)
		# This will be empty

		# def getComp(game, user, pw, files):

		print("Downloading full comp")
		compZip = packager.getComp(games.WOW, self.username, self.password, files)
		self.fullSize = os.stat(compZip.name).st_size

		print("Unzipping %s to %s" % (compZip, self.zgvDir))
		util.unzip(compZip.name, self.compTestDir) # Will create ZygorGuidesViewer

		os.remove(compZip.name)

		self.assertTrue(os.path.exists(self.zgvDir))

		if os.path.exists(self.zgvDir):
			allFiles = util.getAllFiles(self.zgvDir)
			print("Storing MD5's of all downloaded files")
			for f in allFiles:
				self.cache[f] = util.md5(f)

	# This will test a do=comp call followed directly after a
	# full installation. The call should ONLY return a license file
	def test_noDownload(self):

		self.assertTrue(os.path.exists(self.zgvDir))

		licenseExists = os.path.exists(self.zgvDir + "/Licence.lua")
		self.assertTrue(licenseExists)

		# We want it to exist to compare later. It should differ
		if licenseExists:

			files = packager.generateFilesList(self.compTestDir)
			# This will be populated

			print("Downloading 'empty' comp")
			compZip = packager.getComp(games.WOW, self.username, self.password, files)
			
			emptySize = os.stat(compZip.name).st_size
			self.assertTrue(self.fullSize < emptySize)

			print("Unzipping %s to %s" % (compZip, self.zgvDir))
			util.unzip(compZip.name, self.compTestDir) # Will overwrite files in 

			os.remove(compZip.name)

			allFiles = util.getAllFiles(self.zgvDir)
			for f in allFiles:
				name = os.path.basename(f)
				if name != "Licence.lua":
					# This MD5 should match
					if f in self.cache:
						previousMD5 = self.cache[f]
						currentMD5 = util.md5(f)

						self.assertEqual(previousMD5, currentMD5)

					else:
						print("[ERROR] A new file was downloaded when it shouldn't have been: %s" % f)
						self.assertTrue(False)

				else:
					previousMD5 = self.cache[f]
					currentMD5 = util.md5(f)

					self.assertNotEqual(previousMD5, currentMD5)

		else:
			print("[ERROR] Licence.lua does not exist. Did test_fullDownload run first?")

	@classmethod
	def tearDownClass(cls):
		if os.path.exists(cls.zgvDir):
			shutil.rmtree(cls.zgvDir)




if __name__ == "__main__":
	unittest.main()