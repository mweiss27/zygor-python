import unittest

import os
import sys
sys.path.append("..")

import news

class TestNews(unittest.TestCase):

	def test_getNewsEntriesDefault(self):

		entries = news.getNewsEntries()

		self.assertEqual(len(entries), 5)

	def test_getNewsEntriesMore(self):

		count = 10

		entries = news.getNewsEntries(count)

		self.assertEqual(len(entries), count)

if __name__ == "__main__":
	unittest.main()