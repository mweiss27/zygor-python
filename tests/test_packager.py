import unittest

import os
import sys
sys.path.append("..")

import games
import packager
import logging
import util
from query import Query

class TestPackager(unittest.TestCase):

	def test_getLatestVersionNone(self):
		game = None

		result = packager.getLatestVersion(game)

		self.assertIsNone(result)

	def test_getLatestVersionWOW(self):
		game = games.WOW

		result = packager.getLatestVersion(game)

		self.assertIsNotNone(result)

	def test_getLatestVersionESO(self):
		game = games.ESO

		result = packager.getLatestVersion(game)

		self.assertIsNotNone(result)

	def test_getQueryNone(self):
		game = None
		
		# These won't even be used
		user = ""
		pw = ""

		result = packager.getQuery(game, user, pw)

		self.assertIsNone(result)

	def test_getQueryWOWNone(self):
		game = games.WOW

		user = None
		pw = None

		result = packager.getQuery(game, user, pw)

		self.assertIsNone(result)

	def test_getQueryWOWInvalid(self):
		game = games.WOW

		user = "Invalid"
		pw = "Invalid"

		result = packager.getQuery(game, user, pw)

		self.assertIsNotNone(result)

		if result is not None:
			query = Query(result)

			self.assertIsNotNone(query._query)

			if query._query is not None:
				self.assertIsNotNone(query.status)
				if query.status is not None:
					self.assertEqual(query.status, "ERR")

	def test_getQueryWOWValid(self):
		game = games.WOW

		user = "Finaltest3"
		pw = "123456"

		result = packager.getQuery(game, user, pw)

		self.assertIsNotNone(result)

		if result is not None:
			query = Query(result)

			self.assertIsNotNone(query._query)

			if query._query is not None:
				self.assertIsNotNone(query.status)
				if query.status is not None:
					self.assertEqual(query.status, "OK")

	# Compares a Trial account to a non-elite account
	# with the gold guide.
	def test_queryCompareProducts(self):
		game = games.WOW

		user = "Finaltest3"
		pw = "123456"

		result1 = packager.getQuery(game, user, pw)
		self.assertIsNotNone(result1)

		user = "Finaltest4"
		pw = util.encodePassword("P@$$w0rD?/&")

		result2 = packager.getQuery(game, user, pw)
		self.assertIsNotNone(result2)

		if result1 and result2:
			q1 = Query(result1)
			q2 = Query(result2)

			# Compare Trials to wow-gold. Added, none removed
			comp = q1.compare(to=q2)
			self.assertEqual(comp, (True, False, False))

			# Compare wow-gold to Trials. Removed, none added
			comp = q2.compare(to=q1)
			self.assertEqual(comp, (False, True, False))

	# Compares a Trial account to an Elite account
	def test_queryCompareEliteToggle(self):
		game = games.WOW

		user = "Finaltest3"
		pw = "123456"

		result1 = packager.getQuery(game, user, pw)
		self.assertIsNotNone(result1)

		user = "Shenzai"
		pw = "QöbG9scGFuZGE/"

		result2 = packager.getQuery(game, user, pw)
		self.assertIsNotNone(result2)

		if result1 and result2:
			q1 = Query(result1)
			q2 = Query(result2)

			# Compare Trials to Elite (added, none removed)
			comp = q1.compare(to=q2)
			self.assertEqual(comp, (True, False, True))

			# Compare Elite to Trials (removed, none added)
			comp = q2.compare(to=q1)
			self.assertEqual(comp, (False, True, True))

			# Compare a query to itself. No changes
			comp = q1.compare(to=q1)
			self.assertEqual(comp, (False, False, False))

	def test_licenseWOWInvalid(self):
		game = games.WOW

		user = "Invalid"
		pw = "Invalid"

		result = packager.getLicense(game, user, pw)

		self.assertIsNone(result)

	def test_licenseWOWValid(self):
		game = games.WOW

		user = "Finaltest3"
		pw = "123456"

		result = packager.getLicense(game, user, pw)
		
		self.assertIsNotNone(result)

	def test_generateFilesList(self):
		addons = os.path.dirname(os.path.realpath(__file__))
		productDirs = [ "ZygorGuidesViewer" ]

		result = packager.generateFilesList(addons, productDirs)

		self.assertEqual(result.lower(), "F ZygorGuidesViewer/File1.lua##42D108582CAAB856C7234700EB1FF600\r\nF ZygorGuidesViewer/Guides/File2.lua##B24EF3E447A50DCE2EC9E391196EF272\r\n".lower())

if __name__ == "__main__":
	unittest.main()