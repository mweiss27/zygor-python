import os
import sys
sys.path.append("..")

import unittest
import util
from urllib.error import HTTPError

class TestUtil(unittest.TestCase):

	# Tests on util.constructURL

	def test_constructURLNone(self):
		base = None
		params = {}

		result = util.constructURL(base, params)

		self.assertIsNone(result)


	def test_constructURLBaseNoParams(self):
		base = "http://zygorguides.com/blog"
		params = None

		result = util.constructURL(base, params)

		self.assertEqual(result, base)


	def test_constructURLBaseParamsPlain(self):
		base = "http://zygorguides.com/updater/packager.php"
		params = {
			"arg1": "val1",
			"arg2": "val2"
		}

		result = util.constructURL(base, params)

		self.assertTrue(result in [ base + "?arg1=val1&arg2=val2", base + "?arg2=val2&arg1=val1" ])


	def test_constructURLBaseParamsNeedsEncoding(self):
		base = "http://zygorguides.com/updater/packager.php"
		params = {
			"arg": "val that has spaces & invalid symbols?"
		}

		result = util.constructURL(base, params)

		self.assertEqual(result, base + "?arg=val+that+has+spaces+%26+invalid+symbols%3F")


	def test_encodePasswordNone(self):
		pw = None

		result = util.encodePassword(pw)

		self.assertIsNone(result)


	def test_encodePasswordEmpty(self):
		pw = ""

		result = util.encodePassword(pw)

		self.assertEqual(result, "Qö")


	def test_encodePassword(self):
		pw = "test&Password 123?"

		result = util.encodePassword(pw)

		self.assertEqual(result, "QödGVzdCZQYXNzd29yZCAxMjM/")


	def test_openConnectionNone(self):
		url = None

		result = util.openConnection(url)

		self.assertIsNone(result)


	def test_openConnectionValid(self):
		url = "http://zygorguides.com/"

		result = util.openConnection(url)

		self.assertIsNotNone(result)


	def test_openConnectionForbidden(self):
		base = "http://zygorguides.com/updater/packager.php"
		params = {
			"do": "debug_response",
			"code": "403"
		}

		url = util.constructURL(base, params)

		with self.assertRaises(HTTPError) as error:
			result = util.openConnection(url)

		self.assertEqual(error.exception.code, 403)


	def test_openConnectionValidPOST(self):
		url = "http://zygorguides.com/javaclient/dev/postTest.php"
		data = "value=TestPOST"

		result = util.openConnection(url, data)

		self.assertIsNotNone(result)

		if result is not None:
			html = result.read()
			self.assertEqual(html.decode("UTF8").strip(), "VALUE: TestPOST")

	def test_getAllFiles(self):
		zgv = os.path.dirname(os.path.realpath(__file__)) + "/ZygorGuidesViewer"

		files = util.getAllFiles(zgv)

		self.assertEqual(len(files), 2)


	def test_getRelativePathWindows(self):
		fullPath = "C:\\Program Files (x86)\\World of Warcraft\\Interface\\AddOns\\ZygorGuidesViewer\\Arrows\\Arrows.xml"
		addons = "C:\\Program Files (x86)\\World of Warcraft\\Interface\\AddOns"

		relative = util.getRelativePath(fullPath, to=addons)

		self.assertEqual(relative, "ZygorGuidesViewer/Arrows/Arrows.xml")

		# Test it with a trailing \
		fullPath = "C:\\Program Files (x86)\\World of Warcraft\\Interface\\AddOns\\ZygorGuidesViewer\\Arrows\\Arrows.xml"
		addons = "C:\\Program Files (x86)\\World of Warcraft\\Interface\\AddOns\\"

		relative = util.getRelativePath(fullPath, to=addons)

		# getRelativePath removes \ for /
		self.assertEqual(relative, "ZygorGuidesViewer/Arrows/Arrows.xml")


	def test_getRelativePathUnix(self):
		fullPath = "/Applications/World of Warcraft/Interface/AddOns/ZygorGuidesViewer/Arrows/Arrows.xml"
		addons = "/Applications/World of Warcraft/Interface/AddOns"

		relative = util.getRelativePath(fullPath, to=addons)

		self.assertEqual(relative, "ZygorGuidesViewer/Arrows/Arrows.xml")

		# Test it with a trailing /
		fullPath = "/Applications/World of Warcraft/Interface/AddOns//ZygorGuidesViewer/Arrows/Arrows.xml"
		addons = "/Applications/World of Warcraft/Interface/AddOns/"

		relative = util.getRelativePath(fullPath, to=addons)

		self.assertEqual(relative, "ZygorGuidesViewer/Arrows/Arrows.xml")


if __name__ == "__main__":
	unittest.main()