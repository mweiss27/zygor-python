import logging
import base64
import os
import hashlib
import zipfile

import urllib
from urllib.request import urlopen, Request
from urllib.parse import urlencode, quote
from urllib.error import URLError, HTTPError

import config

def constructURL(base, params={}):
	if base is None:
		logging.error("base should not be None in constructURL")
		return None

	if params is None:
		params = {}

	if len(params) > 0 and not base.endswith("?"):
		base = base + "?"

	return base + "%s" % urlencode(params)

# Encodes a plain-text password using the pre-defined prefix, and Base64
def encodePassword(pw):
	if pw is None:
		logging.error("pw must not be None in encodePassword: %s" % pw)
		return None

	return "Qö" + base64.b64encode(bytearray(pw, "UTF8")).decode("UTF8")

# http://stackoverflow.com/a/3431838
def md5(fname):
	if fname is None:
		return None

	hash_md5 = hashlib.md5()
	with open(fname, "rb") as f:
		for chunk in iter(lambda: f.read(4096), b""):
			hash_md5.update(chunk)

	return hash_md5.hexdigest()

# Generates a list of all files in a given directory
def getAllFiles(root):
	files = []

	for root, directories, filenames in os.walk(root):
		for filename in filenames:
			if ".DS_Store" not in filename:
				files.append(os.path.join(root, filename))

	return files

def getRelativePath(fullPath, to=None):
	if to is None:
		return fullPath

	if fullPath.startswith(to):
		replaced = fullPath.replace(to, "", 1)
		while replaced.startswith("/") or replaced.startswith("\\"):
			replaced = replaced[1:]

		return replaced.replace("\\", "/")

	else:
		logging.error("fullPath does not startwith to. No replacement")
		return None


# Creates an HTTPConnection. Does not make any requests.
# Returns: The HTTPConnection object
# Throws: HTTPError if a non-successful response code is returned.
# Throws: URLError if the connection fails
def openConnection(url, data=None):
	if url is None:
		logging.error("url should not be None in openConnection")
		return None

	logging.debug("Opening connection to: %s" % url)

	try:
		logging.debug("Creating a Request object")
		headers = {}
		headers["Content-Length"] = 0

		if data is not None:
			if isinstance(data, dict):
				logging.debug("Converting dict data to urlencoded string")
				data = urlencode(data)
				logging.debug("New data: %s" % data[0:150])
			
			if isinstance(data, str):
				logging.debug("Converting string data to bytes")
				data = bytes(data, "UTF8")

			logging.debug("POSTing data: %s" % data[0:150])

		conn = urlopen(url=url, data=data, timeout=config.SOCKET_TIMEOUT)

		return conn
	except HTTPError as ex:
		logging.error("HTTPError on openConnection. Response Code: %d" % ex.code)
		raise ex
	except URLError as ex:
		logging.error("URLError on openConnection. Reason: %s" % ex.reason)
		raise ex

	except Exception as ex:
		import traceback
		logging.error("'Unhandled' exception raised in openGETConnection")
		traceback.print_exc()
		return None

def readHtml(html):
	if not html:
		return None

	return html.decode("UTF8").strip()

# Unzips a file to the target directory.
# The file structure within the zip will be mirrored 1:1, no
# additional directories will be created.
def unzip(zipFile, targetDir):
	if zipFile is None or targetDir is None:
		logging.error("zipFile and targetDir must not be None in unzip")
		return False

	if not os.path.exists(targetDir):
		os.makedirs(targetDir)

	try:
		zipFile = zipfile.ZipFile(zipFile, "r")
		zipFile.extractall(targetDir)
		zipFile.close()
		return True
		
	except:
		return False









